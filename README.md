# Automation Test 

This is the code base for automation test using Cucumber-Appium for mobile app.

## Getting Started

How to run:

Required node v18.16.0.

Download Jago app from App distribution, and locate it in the project.

Do `npm i` to install packages.

Run by `npx wdio wdio.conf.ts` or `npm run wdio`.