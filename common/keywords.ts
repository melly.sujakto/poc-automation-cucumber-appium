export async function openAppUntilReady() {
  const splashElement = await driver.$(
    "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView"
  );
  await splashElement.waitForDisplayed({ timeout: 10000 });
  await waitUntilPageDoesNotContainElement(splashElement, 50000);
}

export async function openUntilLoginPage() {
  await openAppUntilReady();
  await checkWhetherIsOnGetStartedPageOrNot();
  await shouldContainElement("Welcome to", 10000);
}

export async function loginWithUserCredential() {
  try {
    const isFirstTimeLogin = await checkWhetherFirstTimeLoginOrNot();
    if (isFirstTimeLogin) {
      await loginFirstTime();
    } else {
      await loginNonFirstTime();
    }
    await validateHomepage();
  } finally {
    await sleep(2000);
  }
}

async function loginFirstTime() {
  console.log("Login first time");
  await validateInputPhoneAndPassword();
  await validateUserIdentity();
  await validateOTP();
  await validatePinPage();
}

async function loginNonFirstTime() {
  console.log("Login non first time");
  const usePwElement = await driver.$("~Use Password");
  const isUsePw = await isElementDisplayedUntil(usePwElement, 5000);
  if (isUsePw) {
    await validatePinPage();
  } else {
    await inputPinWithValue("070809");
    const loginNonFirstElement = await driver.$("~Enter your PIN");
    await waitUntilPageDoesNotContainElement(loginNonFirstElement, 30 * 1000);
    await sleep(2000);
  }
}

async function checkWhetherFirstTimeLoginOrNot(): Promise<boolean> {
  console.log("Check First Login Or Not?");
  const loginNonFirstTimeElement = await driver.$("~Enter your PIN");
  const isNonFirst = await isElementDisplayedUntil(
    loginNonFirstTimeElement,
    1000
  );
  if (!isNonFirst) {
    await checkWhetherIsOnGetStartedPageOrNot();
  }
  return !isNonFirst;
}

async function checkWhetherIsOnGetStartedPageOrNot() {
  const getStartedElement = await driver.$("~Get Started");
  const isFirst = await isElementDisplayedUntil(getStartedElement, 5000);
  if (isFirst) {
    console.log("Get Started Page");
    await getStartedElement.click();
    await sleep(10000);
  }
}

async function validateInputPhoneAndPassword() {
  const loginPageElement = await driver.$("~Welcome to");
  const isNeedInputPhonePassword = await isElementDisplayedUntil(
    loginPageElement,
    10000
  );
  if (isNeedInputPhonePassword) {
    console.log("Login Page");
    await inputTextIntoTextField("E-mail/phone number", "628900000733");
    await inputTextIntoTextField("Password", "P@ssw0rd1234");

    const loginBtn = await driver.$("~Log in");
    await loginBtn.click();
  }
}

async function validateUserIdentity() {
  const twoSVElement = await driver.$("~2-Step Verification");
  const isNeed2SV = await isElementDisplayedUntil(twoSVElement, 10000);
  if (isNeed2SV) {
    console.log("2-Step Verification Page");
    const tryAnotheWayBtn = await driver.$("~Try another way");
    await tryAnotheWayBtn.click();

    console.log("Enter your NIK Page");
    const enterYourNikElement = await driver.$("~Enter your NIK");
    await enterYourNikElement.waitForDisplayed({ timeout: 10000 });
    await inputTextIntoCurrentElement("5217391517226380");
    await sleep(1000);
    const nextBtn = await driver.$("~Next");
    await nextBtn.click();

    console.log("Choose a security question Page");
    const chooseSecElement = await driver.$("~Choose a security question");
    await chooseSecElement.waitForDisplayed({ timeout: 10000 });
    const secOptionElement = await driver.$(
      "~What's the name of one of your Pockets?"
    );
    await secOptionElement.click();
    await sleep(1000);
    await inputTextIntoCurrentElement("Daily Expenses");
    await sleep(1000);
    const submitBtn = await driver.$("~Submit");
    await submitBtn.click();
  }
}

async function validateOTP() {
  const otpElement = await driver.$("~Hello there!");
  const isNeedOTP = await isElementDisplayedUntil(otpElement, 5000);
  if (isNeedOTP) {
    console.log("OTP Page");
    await inputTextIntoCurrentElement("000000");
  }
}

async function validatePinPage() {
  const createPinElement = await driver.$("~Create a 6-digit PIN");
  const isCreatePin = await isElementDisplayedUntil(createPinElement, 10000);
  if (!isCreatePin) {
    console.log("Input password before create the PIN");
    await inputTextIntoTextField("Password", "P@ssw0rd1234");
    const loginBtn = await driver.$("~Log in");
    await loginBtn.click();
  }

  const isCreatePin2 = await isElementDisplayedUntil(createPinElement, 5000);
  if (isCreatePin2) {
    console.log("Create a 6-digit PIN Page");
    await inputPinWithValue("070809");

    console.log("Confirm new device PIN Page");
    const confirmPinElement = await driver.$("~Confirm new device PIN");
    await confirmPinElement.waitForDisplayed({ timeout: 5 });
    await inputPinWithValue("070809");

    const skipElement = await driver.$("~Skip");
    await skipElement.waitForDisplayed({ timeout: 5 });
    await skipElement.click();
  }
}

async function validateHomepage() {
  console.log("Homepage");
  (await driver.$("~Shortcuts")).waitForDisplayed({ timeout: 30 * 1000 });
}

export async function upgradeAppVersionAndOpen(appPath: string) {
  await driver.installApp(appPath);
  await driver.launchApp();
  await openAppUntilReady();
}

export async function shouldContainElement(
  accessibilityId: string,
  timeout?: number
) {
  const element = await driver.$(`~${accessibilityId}`);
  await element.waitForDisplayed({ timeout: timeout });
}

export async function shouldNotContainElement(accessibilityId: string) {
  const element = await driver.$(`~${accessibilityId}`);
  if (await element.isDisplayed()) {
    throw Error(`Page should not have contained ${element}`);
  }
}

export async function inputTextIntoTextField(label: string, value: string) {
  await driver.hideKeyboard();
  await waitUntilKeyboardDismissed();
  const textField = await driver.$(
    `//*[@class='android.widget.EditText'][contains(@text, '${label}')] | //*[@class='android.widget.ImageView'][contains(@text, '${label}')] | //XCUIElementTypeTextField[contains(@name, '${label}')]`
  );
  await textField.click();
  await waitUntilKeyboardShown();
  const valueBase64 = Buffer.from(value).toString("base64");
  await driver.setClipboard(valueBase64);
  await driver.pressKeyCode(50, 0x1000 | 0x2000);
  await driver.hideKeyboard();
  await waitUntilKeyboardDismissed();
}

export async function inputTextIntoCurrentElement(value: string) {
  await waitUntilKeyboardShown();
  const valueBase64 = Buffer.from(value).toString("base64");
  await driver.setClipboard(valueBase64);
  await driver.pressKeyCode(50, 0x1000 | 0x2000);
}

export async function waitUntilKeyboardShown() {
  let number = 0;
  while (number != 100) {
    const isKeyboardShown = await driver.isKeyboardShown();
    if (isKeyboardShown) {
      break;
    }
    number + 1;
    await sleep(100);
  }
}

export async function waitUntilKeyboardDismissed() {
  let number = 0;
  while (number != 100) {
    const isKeyboardShown = await driver.isKeyboardShown();
    if (!isKeyboardShown) {
      break;
    }
    number + 1;
    await sleep(100);
  }
}

export async function inputPinWithValue(pin: string) {
  await driver.$(`~${pin[0]}`).click();
  await driver.$(`~${pin[1]}`).click();
  await driver.$(`~${pin[2]}`).click();
  await driver.$(`~${pin[3]}`).click();
  await driver.$(`~${pin[4]}`).click();
  await driver.$(`~${pin[5]}`).click();
}

export async function isElementDisplayedUntil(
  element: WebdriverIO.Element,
  timeout?: number
): Promise<boolean> {
  try {
    await element.waitForDisplayed({ timeout: timeout });
    return true;
  } catch (error) {
    return false;
  }
}

export async function waitUntilPageDoesNotContainElement(
  element: WebdriverIO.Element,
  timeout: number
) {
  let initMS = new Date().getTime();

  function validateTimeout() {
    let curMS = new Date().getTime();
    let dif = curMS - initMS;
    let valid = dif < timeout;
    console.log("==> ", initMS, curMS, dif, valid);
    if (!valid) {
      throw Error(`Element ${element} did not disappear in ${timeout}ms`);
    }
    return valid;
  }

  while (validateTimeout()) {
    const present = await element.isDisplayed();
    if (!present) {
      break;
    }
    await sleep(100);
  }
}

export async function tapOnButtonWithName(btnName: string) {
  const btn = await driver.$(`~${btnName}`);
  await btn.click();
  await sleep(1000);
}

export const sleep = (timeout: number) =>
  new Promise((res) => setTimeout(res, timeout));
