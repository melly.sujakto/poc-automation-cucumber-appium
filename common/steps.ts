import { Given, When, Then } from "@wdio/cucumber-framework";
import {
  inputTextIntoTextField,
  loginWithUserCredential,
  openUntilLoginPage,
  openAppUntilReady,
  shouldContainElement,
  shouldNotContainElement,
  tapOnButtonWithName,
  upgradeAppVersionAndOpen,
} from "./keywords";

Given("I open until login page", async () => {
  return Promise.resolve(await openUntilLoginPage());
});

Given("I open app until ready", async () => {
  return Promise.resolve(await openAppUntilReady());
});

Given("I log in with user credential", async () => {
  return Promise.resolve(await loginWithUserCredential());
});

When(
  /^I update the app with (.*) and open it$/,
  async function (appPath: string) {
    await upgradeAppVersionAndOpen(appPath);
  }
);

When(
  /^I input text into text field (.*) with value (.*)$/,
  async function (label: string, value: string) {
    await inputTextIntoTextField(label, value);
  }
);

When(/^I tap on button with name (.*)$/, async (btnName: string) => {
  await tapOnButtonWithName(btnName);
});

When(/^I should not see a (.*) element$/, async (accessibilityId: string) => {
  await shouldNotContainElement(accessibilityId);
});

When(/^I should see a (.*) element$/, async (accessibilityId: string) => {
  await shouldContainElement(accessibilityId);
});

Then(/^I should see a error with message (.*)$/, async (message) => {
  const errMsg = await driver.$(`~${message}`);
  await errMsg.waitForDisplayed({ timeout: 10 * 1000 });
});
