Feature: Log in

    Scenario Outline: Log In With Wrong Password Should Be Error
        Given I open until login page
        When I input text into text field <labelPhone> with value <incorrectPhone>
        And I input text into text field <labelPw> with value <incorrectPassword>
        And I tap on button with name <btnName>
        Then I should see a error with message <errMsg>
        And I should still on the login page

        Examples:
            | labelPhone          | labelPw  | incorrectPhone | incorrectPassword | btnName | errMsg                        |
            | E-mail/phone number | Password | 080000000      | xxxxxxxx          | Log in  | Inputted info is not matched. |


    Scenario Outline: Log In With Correct Phone And Password Should Be Succeeded
        Given I open until login page
        When I input text into text field <labelPhone> with value <correctPhone>
        And I input text into text field <labelPw> with value <correctPassword>
        And I tap on button with name <btnName>
        Then I should move to the 2SV page

        Examples:
            | labelPhone          | labelPw  | correctPhone | correctPassword | btnName |
            | E-mail/phone number | Password | 628900000733 | P@ssw0rd1234    | Log in  |
