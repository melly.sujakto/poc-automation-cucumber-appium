Feature: Log in with multiple versions

    Scenario: Log in with current app and update to v.8.18.0
        #Old Version
        Given I open app until ready
        Then I should not see a Enter your PIN element
        When I log in with user credential
        Then I should see a Shortcuts element
        #New Version
        When I update the app with 8_18.apk and open it
        Then I should see a Enter your PIN element
        When I log in with user credential
        Then I should see a Shortcuts element