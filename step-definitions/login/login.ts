import { Given, When, Then, setDefaultTimeout } from "@wdio/cucumber-framework";
import { shouldContainElement } from "../../common/keywords";
setDefaultTimeout(3600 * 1000);

Then("I should still on the login page", async () => {
  await shouldContainElement("Welcome to", 10000);
});

Then("I should move to the 2SV page", async () => {
  await shouldContainElement("2-Step Verification", 10000);
});
